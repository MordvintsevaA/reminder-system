const express = require("express");
const mongoose = require('mongoose');
const UserModel = require("./user/user");
const Telegram = require("./Telegram/Telegram");

mongoose.connect('mongodb://localhost:27017/reminder-system?replicaSet=rs0', {useNewUrlParser: true, useUnifiedTopology: true}).then(() => {
    const pipeline = [
        {
            '$match': {
                '$and': [
                    { "updateDescription.updatedFields.status": { $exists: true } },
                    { operationType: "update" }]
            }
        }
    ]

    const changeStream = UserModel.watch(pipeline);
    changeStream.on('change', (change) => {
        const id = change.documentKey;
        const newStatus = change.updateDescription.updatedFields.status;
        console.log(id, newStatus);
        Telegram.sendReminder(id).then(console.log('Reminder to ', id, ' is sent'));
    })

}).catch(console.log);

const app = express();
const port = 3000;

app.get('/', async (req, res) => {
    try {
        console.log(await Telegram.sendMessage('280374540','Hey!'));
        res.send({status: "ok"});
    } catch (e) {
        res.send({error: e.message});
    }
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})

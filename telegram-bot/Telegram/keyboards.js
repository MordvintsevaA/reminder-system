const emptyKeyboard = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
        ]
    })
}

const deviceKeyboard = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{text: 'Добавить устройство', callback_data: 'addDevice'}],
            [{text: 'Изменить устройство', callback_data: 'changeDevice'}],
            [{text: 'Удалить устройство', callback_data: 'deleteDevice'}]
        ]
    })
}

const reminderKeyboard = {
    reply_markup: JSON.stringify({
        inline_keyboard: [
            [{text: 'Вывести список напоминаний', callback_data: 'remindersList'}],
            [{text: 'Добавить напоминание', callback_data: 'addReminder'}],
            [{text: 'Удалить напоминание', callback_data: 'deleteReminder'}]
        ]
    })
}

module.exports = {deviceKeyboard, reminderKeyboard, emptyKeyboard}
const TG = require('node-telegram-bot-api')
const {getInsideReminders} = require("../reminder/usecases");
const {getOutsideReminders} = require("../reminder/usecases");
let {deleteReminder} = require("../reminder/usecases");
const {getAllReminders} = require("../reminder/usecases");
const {createReminder} = require("../reminder/usecases");
const {reminderKeyboard} = require("./keyboards");
const {findUserByTelegramId} = require("../user/usecases");
const {deleteUser} = require("../user/usecases");
const {updateUserMac} = require("../user/usecases");
const {createUser} = require("../user/usecases");
const {findUserById} = require("../user/usecases");
const {emptyKeyboard} = require("./keyboards");
const {deviceKeyboard} = require("./keyboards");
let answerCallbacks = {};

class Telegram {
    constructor() {
        this.bot = new TG(process.env.BOT_TOKEN, { polling: true });

        this.bot.on('message', function (message) {
            let callback = answerCallbacks[message.chat.id];
            if (callback) {
                delete answerCallbacks[message.chat.id];
                return callback(message);
            }
        });
        this.bot.onText(/^\/start$/, this.onStart)
        this.bot.onText(/^\/device$/, this.onDevice)
        this.bot.onText(/^\/reminder/, this.onReminder)
        this.bot.on('callback_query', this.onCallback)
    }

    onStart = (msg) => {
        this.bot.sendMessage(msg.chat.id, `Привет, ${msg.from.first_name}! С помощью этого бота ты можешь добавить своё Bluetooth устройство к домашней системе напоминаний.\nДля управления устройствами введи команду /device\nДля управления заметками введи команду /reminders`)
            .then(console.log).catch(console.log);
    }

    onDevice = (msg) => {
        this.bot.sendMessage(msg.chat.id, `Выберите действие`, deviceKeyboard).then(console.log).catch(console.log)
    }

    onReminder = async (msg) => {
        const user = await findUserByTelegramId(msg.from.id);
        if (!user.confirmed) {
            this.bot.sendMessage(user.chatId, "Ваше устройство не подтверждено. Почему могла возникнуть эта проблема:\n - на устройстве выключен Bluetooth LE\n - устройство далеко от станции Raspberry Pi.").then(console.log).catch(console.log);
            return;
        }
        this.bot.sendMessage(msg.chat.id, `Выберите действие`, reminderKeyboard).then(console.log).catch(console.log)
    }

    sendMessage = async (chatId, text) => {
        this.bot.sendMessage(chatId, text).then(console.log).catch(console.log);
    }

    sendReminder = async (id) => {
        const user = await findUserById(id);
        if (!user.confirmed) return;

        let text;
        let reminders;

        if (user.status === "OUTSIDE") {
            reminders = await getOutsideReminders();
            console.log(reminders.length)
            if (reminders.length === 0) return;
            text = "Мы потеряли вас с наших радаров! Если вы вышли на улицу, не забудьте:\n";
        } else if (user.status === "INSIDE") {
            reminders = await getInsideReminders();
            console.log(reminders.length)
            if (reminders.length === 0) return;
            text = "С возвращением! Дома вас ждут дела:\n"
        }

        for (const reminder of reminders) {
            text += "• " + reminder.text + "\n";
        }

        this.bot.sendMessage(user.chatId, text).then(console.log).catch(console.log);
    }

    onCallback = async (msg) => {
        switch (msg.data) {
            case 'addDevice':
                await this.addDevice(msg);
                break;
            case 'changeDevice':
                await this.changeDevice(msg);
                break;
            case 'deleteDevice':
                await this.deleteDevice(msg);
                break;
            case 'remindersList':
                await this.remindersList(msg);
                break;
            case 'addReminder':
                await this.addReminder(msg);
                break;
            case 'deleteReminder':
                await this.deleteReminder(msg);
                break;
            default:
                this.bot.editMessageReplyMarkup(emptyKeyboard, {chat_id: msg.message.chat.id, message_id: msg.message.message_id}).then(console.log).catch(console.log)
                this.bot.sendMessage(msg.message.chat.id, `Произошла внутренняя ошибка. Попробуйте ещё раз позже.`).then(console.log).catch(console.log)
                break;
        }
    }

    addDevice = async (msg) => {
        const bot = this.bot;
        bot.editMessageReplyMarkup(emptyKeyboard, {chat_id: msg.message.chat.id, message_id: msg.message.message_id}).then(console.log).catch(console.log)
        // check if user is already exists
        const user = await findUserByTelegramId(msg.from.id);

        if (user) {
            bot.sendMessage(msg.message.chat.id, "Вы уже добавили устройство с MAC-адресом " + user.MAC).then(console.log).catch(console.log)
            return;
        }

        bot.sendMessage(msg.message.chat.id, "Введите MAC-адрес устройства в формате FF:FF:FF:FF:FF:FF").then(function () {
            answerCallbacks[msg.message.chat.id] = function (answer) {
                const answerText = answer.text.trim();
                const regex = new RegExp("^[0-9a-f]{1,2}([\.:-])(?:[0-9a-f]{1,2}\\1){4}[0-9a-f]{1,2}$", "gi");
                const mac = answerText.match(regex)

                if (!mac) {
                    bot.sendMessage(msg.message.chat.id, "Неправильно введён MAC-адрес. Попробуйте ещё раз воспользоваться командой /device").then(console.log).catch(console.log);
                } else {
                    createUser(msg.from.id, msg.message.chat.id, msg.from.first_name, mac[0])
                    bot.sendMessage(msg.message.chat.id, "MAC-адрес устройства сохранён").then(console.log).catch(console.log);
                }
            }
        });
    }

    changeDevice = async (msg) => {
        const bot = this.bot;
        bot.editMessageReplyMarkup(emptyKeyboard, {chat_id: msg.message.chat.id, message_id: msg.message.message_id}).then(console.log).catch(console.log)
        // check if user is already exists
        const user = await findUserByTelegramId(msg.from.id);

        if (!user) {
            bot.sendMessage(msg.message.chat.id, "Вы ещё не добавили устройство. Попробуйте ещё раз воспользоваться командой /device").then(console.log).catch(console.log);
            return;
        }

        bot.sendMessage(msg.message.chat.id, "Сохранённый MAC-адрес вашего устройства " + user.MAC + ". Введите новый MAC-адрес устройства в формате FF:FF:FF:FF:FF:FF").then(function () {
            answerCallbacks[msg.message.chat.id] = function (answer) {
                const answerText = answer.text.trim();
                const regex = new RegExp("^[0-9a-f]{1,2}([\.:-])(?:[0-9a-f]{1,2}\\1){4}[0-9a-f]{1,2}$", "gi");
                const mac = answerText.match(regex)

                if (!mac) {
                    bot.sendMessage(msg.message.chat.id, "Неправильно введён MAC-адрес. Попробуйте ещё раз воспользоваться командой /device").then(console.log).catch(console.log);
                } else {
                    updateUserMac(msg.from.id, mac[0]);
                    bot.sendMessage(msg.message.chat.id, "MAC-адрес устройства сохранён").then(console.log).catch(console.log);
                }
            }
        });
    }

    deleteDevice = async (msg) => {
        this.bot.editMessageReplyMarkup(emptyKeyboard, {chat_id: msg.message.chat.id, message_id: msg.message.message_id}).then(console.log).catch(console.log)
        // check if user is already exists
        const user = await findUserByTelegramId(msg.from.id);

        if (!user) {
            this.bot.sendMessage(msg.message.chat.id, "Вы ещё не добавили устройство. Попробуйте ещё раз воспользоваться командой /device").then(console.log).catch(console.log);
            return;
        }

        await deleteUser(msg.from.id);

        this.bot.sendMessage(msg.message.chat.id, "Сохранённый MAC-адрес вашего устройства " + user.MAC + " был удалён")
            .then(console.log)
            .catch(console.log);
    }

    remindersList = async (msg) => {
        this.bot.editMessageReplyMarkup(emptyKeyboard, {chat_id: msg.message.chat.id, message_id: msg.message.message_id}).then(console.log).catch(console.log)

        const {inside, outside} = (await getAllReminders());

        let text = "INSIDE 🏡\n";
        for (const reminder of inside) {
            text += ("• " + reminder.text + "\n");
        }

        text += "OUTSIDE 🌲\n";
        for (const reminder of outside) {
            text += ("• " + reminder.text + "\n");
        }

        this.bot.sendMessage(msg.message.chat.id, text).then(console.log).catch(console.log)
    }

    addReminder = async (msg) => {
        const bot = this.bot;
        bot.editMessageReplyMarkup(emptyKeyboard, {chat_id: msg.message.chat.id, message_id: msg.message.message_id}).then(console.log).catch(console.log)
        bot.sendMessage(msg.message.chat.id, `Чтобы добавить заметку, отправьте сообщение в формате "outside\\inside [text]"\nПример: outside зайти в магазин`).then(function () {
                answerCallbacks[msg.message.chat.id] = async function (answer) {
                    const answerTokens = answer.text.trim().split(' ');
                    const condition = answerTokens[0].toUpperCase()

                    if (condition !== 'OUTSIDE' && condition !== 'INSIDE') {
                        bot.sendMessage(msg.message.chat.id, "Не удалось распарсить текст. Проверьте, что вы не забыли указать условие inside/outside, и выполните команду /reminders ещё раз.").then(console.log).catch(console.log);
                        return;
                    }
                    answerTokens.shift();
                    await createReminder(msg.from.id, answerTokens.join(" "), condition);

                    bot.sendMessage(msg.message.chat.id, "Заметка сохранена.").then(console.log).catch(console.log);
                }
            }
        )
    }

    deleteReminder = async (msg) => {
        const bot = this.bot;
        bot.editMessageReplyMarkup(emptyKeyboard, {chat_id: msg.message.chat.id, message_id: msg.message.message_id}).then(console.log).catch(console.log)
        const {inside, outside} = (await getAllReminders());
        let counter = 1;

        let text = "INSIDE 🏡\n";
        for (const reminder of inside) {
            text += (counter.toString() + ". " + reminder.text + "\n");
            counter += 1;
        }

        text += "OUTSIDE 🌲\n";
        for (const reminder of outside) {
            text += (counter.toString() + ". " + reminder.text + "\n");
            counter += 1;
        }

        bot.sendMessage(msg.message.chat.id, 'Чтобы удалить заметку, введите её номер в списке:\n' + text).then(function () {
                answerCallbacks[msg.message.chat.id] = async function (answer) {
                    const answerText = answer.text.trim();
                    const number = parseInt(answerText);

                    if (!number) {
                        bot.sendMessage(msg.message.chat.id, "Проверьте правильность ввода номера заметки и попробуйте ещё раз воспользоваться командой /reminders").then(console.log).catch(console.log);
                        return;
                    }

                    console.log(inside, outside);

                    if (number <= inside.length) {
                        await deleteReminder(inside[number - 1]._id);
                        bot.sendMessage(msg.message.chat.id, "Заметка удалена.").then(console.log).catch(console.log);
                    } else if (number > inside.length && number <= (inside.length + outside.length)) {
                        await deleteReminder(outside[number - inside.length - 1]._id);
                        bot.sendMessage(msg.message.chat.id, "Заметка удалена.").then(console.log).catch(console.log);
                    } else {
                        bot.sendMessage(msg.message.chat.id, "Проверьте правильность ввода номера заметки и попробуйте ещё раз воспользоваться командой /reminders").then(console.log).catch(console.log);
                    }
                }
            }
        )
    }
}

module.exports = new Telegram();

const UserModel = require("./user");

const createUser = async (telegramId, chatId, name, MAC) => {
    return await UserModel.create({
        telegramId,
        chatId,
        name,
        MAC
    })
}

const updateUserMac = async (telegramId, MAC) => {
    await UserModel.updateOne({telegramId: telegramId.toString()}, {MAC: MAC, status: "OUTSIDE", confirmed: false});
}

const deleteUser = async (telegramId) => {
    await UserModel.deleteOne({telegramId: telegramId.toString()});
}

const findUserByTelegramId = async (telegramId) => {
    return (await UserModel.findOne({telegramId: telegramId}));
}

const findUserById = async (id) => {
    return (await UserModel.findOne({_id: id}));
}

module.exports = {
    createUser,
    updateUserMac,
    deleteUser,
    findUserByTelegramId,
    findUserById
}
const mongoose = require("mongoose");

const User = new mongoose.Schema({
    telegramId: {type: String, required: true},
    chatId: {type: String, required: true},
    name: {type: String, required: true},
    MAC: String,
    confirmed: {type: Boolean, default: false},
    status: {type: String, enum: ['INSIDE', 'OUTSIDE'], default: 'OUTSIDE'},
    lastSeen: {type: Date, default: Date.now()}
})

module.exports = mongoose.model("User", User)
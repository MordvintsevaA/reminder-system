const mongoose = require("mongoose");

const Reminder = new mongoose.Schema({
    owner: {type: String, required: true}, // telegram id
    text: {type: String, required: true},
    condition: {type: String, enum: ['INSIDE', 'OUTSIDE'], required: true},
    date: {type: Date, default: Date.now()}
})

module.exports = mongoose.model("Reminder", Reminder)
const ReminderModel = require("./reminder");

const createReminder = async (telegramId, text, condition) => {
    return await ReminderModel.create({
        owner: telegramId,
        text,
        condition
    })
}

const deleteReminder = async (id) => {
    await ReminderModel.deleteOne({_id: id});
}

const getAllReminders = async () => {
    const inside = await ReminderModel.find({condition: "INSIDE"});
    const outside = await ReminderModel.find({condition: "OUTSIDE"});
    return {inside, outside};
}

const getInsideReminders = async () => {
    return ReminderModel.find({condition: "INSIDE"});
}

const getOutsideReminders = async () => {
    return ReminderModel.find({condition: "OUTSIDE"});
}

module.exports = {
    createReminder,
    deleteReminder,
    getAllReminders,
    getInsideReminders,
    getOutsideReminders
}
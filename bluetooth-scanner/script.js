let noble = require('noble');
const mongoose = require('mongoose');
const {updateLastSeenTime} = require("./user/usecases");
const {getUsersList} = require("./user/usecases");
const schedule = require('node-schedule');
const {findUserById} = require("./user/usecases");

mongoose.connect('mongodb://localhost:27017/reminder-system?replicaSet=rs0', {useNewUrlParser: true, useUnifiedTopology: true}).then().catch(console.log);

let macs;
let usersMap;

getUsersList().then((userMap) => {
    usersMap = userMap;
    macs = Object.keys(userMap).map(function(key) {
        return userMap[key];
    });
    console.log("MACs to scan", macs)
});

noble.on('discover', function(peripheral) {
  const  macAddress = peripheral.address;
  if (macs.includes(macAddress))
  {
      const id = Object.keys(usersMap)
          .map(function(key) {
              return usersMap[key] = macAddress ? key : null;
          })
          .filter((id) => { return id != null });

      updateLastSeenTime(Date.now(), id[0]).then()
  }
});
noble.on('stateChange',  function(state) {
    if (state!=="poweredOn") return;
    console.log("Starting scan...");
    noble.startScanning([], true);
});
noble.on('scanStart', function() { console.log("Scanning started."); });
noble.on('scanStop', function() { console.log("Scanning stopped.");});

schedule.scheduleJob('*/15 * * * * *', () => {
    console.log('Start updating devices status');
    getUsersList().then(async (userMap) => {
        usersMap = userMap;
        macs = Object.keys(userMap).map(function(key) {
            return userMap[key];
        });
        console.log('Updated MACs to scan', macs)
        const ids = Object.keys(usersMap);
        for (const id of ids) {
            const user = await findUserById(id);
            const timeDif = (Date.now() - user.lastSeen) / (1000 * 60); // in minutes
            if (timeDif > 1 && user.status === "INSIDE") {
                user.status = "OUTSIDE";
                user.save();
                console.log("User's " + user._id + " status has changed to OUTSIDE")
            }
        }

        console.log("Stop updating devices status")
    });
});
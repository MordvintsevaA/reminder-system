const UserModel = require("./user");

const getUsersList = async () => {
    const users = (await UserModel.find({}));
    const userMap = {};
    users.forEach((user) => {
        userMap[user._id] = user.MAC.toLowerCase();
    });
    return userMap;
}

const updateLastSeenTime = async (time, id) => {
    const user = await UserModel.findOne({_id: id})
    user.lastSeen = time;
    if (user.status === "OUTSIDE")
    {
        user.status = "INSIDE";
    }
    user.confirmed = true;
    await user.save();
}

const findUserById = async (id) => {
    return (await UserModel.findOne({_id: id}));
}

module.exports = {
    getUsersList,
    updateLastSeenTime,
    findUserById
}
